﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HelloWebForm.aspx.cs" 
    Inherits="DemoWeb.HelloWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <asp:Label ID="Label1" runat="server" Text="Votre nom : "></asp:Label>
            <asp:TextBox ID="txtNom" Text="plop" runat="server"></asp:TextBox>
            <asp:Button ID="bntOk" runat="server" OnClick="bntOk_Click" Text="ceci est un bouton OK" />
            <asp:Button ID="btnSouligne" OnClick="btnSouligne_Click" runat="server" Text="cliquez sur Souligner afin de soulinger le texte" />
            <asp:Button ID="btnItalique" OnClick="btnItalique_Click" runat="server" Text="En activant l'Italique vous verrez la différence" />
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class Accueil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Lecture dans une session :
            string login = (string)Session["login"];

            if (login != null)
            {
                lblLogin.Text = "Vous êtes " + login;
            }
            else
            {
                lblLogin.Text = "On ne se conait pas...";
            }
        }
    }
}
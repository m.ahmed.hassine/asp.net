﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWeb
{
    [Serializable]
    public class Personne
    {
        public string Nom { get; set; }
        public int Age { get; set; }
    }
}
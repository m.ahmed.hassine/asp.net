﻿<%@ Page Language="C#"%>

<%
    Response.Write("<!DOCTYPE html>\n");
    Response.Write("<HTML>\n");
    Response.Write("\t<HEAD>\n");
    Response.Write("\t\t<link href='css/styles.css' rel='stylesheet' />");
    Response.Write("\t</HEAD>\n");
    Response.Write("\t<BODY>\n");
    Response.Write("\t\t<h1>Hello web !</h1>\n");

    Response.Write(DateTime.Now + "<br/>");

    string[] saisons = { "printemps", "été", "automne", "hiver" };

    foreach(string saison in saisons)
    {
        Response.Write(saison + "\n");
        Response.Write("<br/>");
    }

    Response.Write("\t</BODY>\n");
    Response.Write("</HTML>\n");
%>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class Meteo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // url demandée :
            // Meteo.aspx?cp=75

            // je récupère le paramètre cp dans l'url :
            int cp = int.Parse(Request["cp"]);

            // je récupère les infos à afficher :
            switch(cp)
            {
                case 75:
                    lblVille.Text = "Paris";
                    lblTemperature.Text = "15";
                    break;
                case 69:
                    lblVille.Text = "Lyon";
                    lblTemperature.Text = "19";
                    break;
                case 13:
                    lblVille.Text = "Marseille";
                    lblTemperature.Text = "25";
                    break;
                case 33:
                    lblVille.Text = "Bordeaux";
                    lblTemperature.Text = "18";
                    break;
            }

        }
    }
}
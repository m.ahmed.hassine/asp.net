﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
<head>
    <title>ASP.NET Rules</title>
    <link href="css/styles.css" rel="stylesheet" />
</head>
<body>
    <h1><%=DateTime.Now%></h1>
    <br />
    
    <%
        string[] saisons = { "printemps", "été", "automne", "hiver" };
    %>
    <ul>
        <%
            foreach (string saison in saisons)
            {
            %>
                <li><%=saison%></li>
            <%
            }
        %>
    </ul>
</body>
</html>
